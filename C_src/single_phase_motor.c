// Differences from Sean J. Miller on element14
// Since its conception, the links have changed and have since been lost!

// Builds for boards that help me learn (beagleboard.org)
// 08/2024
// Seth

#include <stdio.h>
#include <unistd.h>
// #include <gpiod.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

void setup_GPIO_One() {
    FILE *bright;
    bright = fopen("/sys/class/leds/m1_high/brightness", "w");
    fseek(bright, 0, SEEK_SET);
    fprintf(bright, "%d", 1);
    fflush(bright);
}

void setupPWM() {
    FILE *period, *pwm;
    pwm = fopen("/dev/beagle/P9_16/enable", "w");
    fseek(pwm, 0, SEEK_SET);
    fprintf(pwm, "%d", 1);
    fflush(pwm);
//    fclose(pwm);

    printf("Period...!\n");
    period = fopen("/dev/beagle/P9_16/period", "w");
    fseek(period, 0, SEEK_SET);
    fprintf(period, "%d", 2500);
    fflush(period);
//    fclose(period);
}

void pwm_duty(int the_duty_multiplier) {
    FILE *duty;
    duty = fopen("/dev/beagle/P9_16/duty_cycle", "w");
    fseek(duty, 0, SEEK_SET);
    fprintf(duty, "%d", 100 * the_duty_multiplier);
    fflush(duty);
//    fclose(duty);
}

int main() {
    printf("Setting up\n");
    int ii = 0;
    int up = 0;
    setupPWM();
    setup_GPIO_One();

    while(1) {
        if (up == 1) ii++; else ii--;
        if ((ii) > 20) {
            up = 0;
            int bright = fwrite("/sys/class/leds/m1_high/brightness", 0);
            pwm_duty(10);
        }
        usleep(250000);

        if (ii < 2) {
            up = 1;
            int bright = fwrite("/sys/class/leds/m1_high/brightness", 1);
            pwm_duty(10);
        }

        usleep(250000);
    }
    pwm_duty(0);
    return 0;
}
